package com.example.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
public class Controller {

    @RequestMapping("/hello.do")
    public String hello(HttpServletRequest request){
        String userId=JwtHelper.getUserId();
        String role=JwtHelper.getRole();
        return "你好 "+userId+"，你的角色是："+role;
    }

    @RequestMapping("/login.do")
    public String login(String username, String password,
                        HttpServletRequest request, HttpServletResponse response){
        if("tom".equals(username) && "tom".equals(password)){
            //模拟userId
            String userId="12345";
            String role="patient";
            String authHeader=JwtHelper.createJwt(userId,role);
            response.addHeader(JwtHelper.HEADER_KEY, authHeader);
            Cookie cookie = new Cookie(JwtHelper.HEADER_KEY, authHeader);
            cookie.setPath(request.getContextPath());
            response.addCookie(cookie);
            return "success";
        }
        return "error";
    }

}
