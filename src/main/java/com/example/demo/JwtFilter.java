package com.example.demo;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

public class JwtFilter implements javax.servlet.Filter {

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        final HttpServletRequest request = (HttpServletRequest) req;
        final HttpServletResponse response = (HttpServletResponse) res;

        String uri=request.getRequestURI();
        //静态资源不拦截
        if(uri.endsWith(".html") || uri.endsWith(".js") || uri.endsWith(".css")){
        	 chain.doFilter(request, response);
        	 return;
        }
        //登录请求不拦截
        if(uri.endsWith("login.do")){
            chain.doFilter(request, response);
            return;
        }

        //从请求头部获取jwt信息
        String authHeader = request.getHeader(JwtHelper.HEADER_KEY);
        //从url中获取jwt信息
        if(authHeader==null){
            authHeader=request.getParameter(JwtHelper.HEADER_KEY);
        }
        //从cookie中获取jwt信息
        if(authHeader==null){
            Cookie[] cookies=request.getCookies();
            if(cookies!=null) {
                for (Cookie cookie : cookies) {
                    if (JwtHelper.HEADER_KEY.equals(cookie.getName())) {
                        authHeader = cookie.getValue();
                    }
                }
            }
        }

        //jwt不存在，跳转到登录页
        if (authHeader == null) {
            response.sendRedirect("/login.html");
            return;
        //解析jwt
        }else{
            Claims claim = null;
            try {
                claim = Jwts.parser()
                        .setSigningKey(JwtHelper.SECRET)
                        .parseClaimsJws(authHeader)
                        .getBody();
            }catch (ExpiredJwtException e){
                System.out.println("登录超时");
                response.sendRedirect("/login.html");
                return;
            }
            //获取userId，放入线程缓存中
            String userId = (String) claim.get("userId");
            String role = (String) claim.get("role");
            JwtHelper.setUserId(userId);
            JwtHelper.setRole(role);

            //大于超时时间的一半，更新jwt，重设超时时间
            Date expirationDate=claim.getExpiration();
            if( (expirationDate.getTime()-System.currentTimeMillis()) < (JwtHelper.EXPIRE_TIME/2) ){
                authHeader=JwtHelper.createJwt(userId,role);
                response.addHeader(JwtHelper.HEADER_KEY, authHeader);
                Cookie cookie = new Cookie(JwtHelper.HEADER_KEY, authHeader);
                cookie.setPath(request.getContextPath());
                response.addCookie(cookie);
            }
        }

        chain.doFilter(req, res);
    }

    @Override
    public void destroy(){
        //按照阿里的规范，threadLocal对象必须要手动回收
        JwtHelper.removeUserId();
        JwtHelper.removeRole();
    }
}
