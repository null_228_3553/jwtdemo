package com.example.demo;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class JwtHelper {
    /**
     * cookie名
     */
    public static final String HEADER_KEY = "baqi_jwt";
    /**
     * 密钥
     */
    public static final String SECRET = "baqi_jwt";
    /**
     * 超时时间30分钟
     */
    public static final long EXPIRE_TIME = 5 * 1000;

    private static ThreadLocal<String> userIdLocal = new ThreadLocal<String>();
    private static ThreadLocal<String> roleLocal = new ThreadLocal<String>();

    public static void setUserId(String userId) {
        userIdLocal.set(userId);
    }

    public static String getUserId() {
        return userIdLocal.get();
    }

    public static void removeUserId() {
        userIdLocal.remove();
    }

    public static void setRole(String role) {
        roleLocal.set(role);
    }

    public static String getRole() {
        return roleLocal.get();
    }

    public static void removeRole() {
        roleLocal.remove();
    }

    public static String createJwt(String userId, String role) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("userId", userId);
        map.put("role", role);
        String authHeader = Jwts.builder()
                .setClaims(map)
//                    .setSubject(userId)
                //30分钟失效
                .setExpiration(new Date(System.currentTimeMillis() + JwtHelper.EXPIRE_TIME))
                .signWith(SignatureAlgorithm.HS256, JwtHelper.SECRET)
                .compact();
        return authHeader;
    }
}
